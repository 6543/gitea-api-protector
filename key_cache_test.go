package main

import (
	"testing"
	"time"
)

func TestKeyCacheHasKey(t *testing.T) {
	cache := NewKeyCache(10*time.Millisecond, 20*time.Millisecond)

	cache.AddKey("test")
	hasKey := cache.HasKey("test")

	if !hasKey {
		t.Error("Cache does not contain added key")
		return
	}
}

func TestKeyCacheCleanup(t *testing.T) {
	cache := NewKeyCache(10*time.Millisecond, 20*time.Millisecond)

	cache.AddKey("test")
	cache.AddKey("test1")
	cache.AddKey("test2")

	for i := 0; i < 3; i++ {
		time.Sleep(10 * time.Millisecond)

		if !(cache.HasKey("test") && cache.HasKey("test1") && cache.HasKey("test2")) {
			t.Error("Added keys have been removed too early")
		}
	}

	time.Sleep(40 * time.Millisecond)

	if cache.HasKey("test") || cache.HasKey("test1") || cache.HasKey("test2") {
		t.Error("Keys haven't been removed from cache when they should")
	}
}
