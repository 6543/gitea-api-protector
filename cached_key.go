package main

import (
	"sync"
	"time"
)

func NewCachedKey() *CachedKey {
	return &CachedKey{
		hit:     time.Now(),
		hitChan: make(chan bool),
		quit:    make(chan bool),
	}
}

// CachedKey represents a cached API key
type CachedKey struct {
	hit     time.Time
	hitMx   sync.RWMutex
	hitChan chan bool
	quit    chan bool
}

func (k *CachedKey) LastHit() time.Time {
	k.hitMx.RLock()
	defer k.hitMx.RUnlock()

	return k.hit
}

func (k *CachedKey) Hit() {
	k.hitChan <- true
}

func (k *CachedKey) Stop() {
	k.quit <- true
}

func (k *CachedKey) Start() {
	go func() {
		for{
			select {
			case <-k.quit:
				return
			case <-k.hitChan:
				k.hitMx.Lock()
				k.hit = time.Now()
				k.hitMx.Unlock()
			}
		}
	}()
}
