package main

import (
	"bytes"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHTTPServeNotMerged(t *testing.T) {
	requestBody := `{
"pull_request": {
    "user": {
        "id": 3
     },
     "merged": false
},
"action": "closed",
"secret": "johnny"
}`

	request := httptest.NewRequest(
		"GET",
		"/verify",
		bytes.NewReader(
			[]byte(requestBody),
		),
	)

	userStore := &InMemoryUserIDStore{
		ids: make(map[uint32]bool),
	}
	response := httptest.NewRecorder()

	handler := &VerifyUserHandler{
		secret: "johnny",
		db:     userStore,
	}

	handler.ServeHTTP(response, request)

	status := response.Result().StatusCode
	if status != http.StatusOK {
		t.Error(
			fmt.Sprintf(
				"Invald status code returned: %d - %s",
				status,
				http.StatusText(status),
			),
		)
	}
}

func TestHTTPServeMerged(t *testing.T) {
	requestBody := `{
"pull_request": {
    "user": {
        "id": 3
     },
     "merged": true
},
"action": "closed",
"secret": "johnny"
}`

	request := httptest.NewRequest(
		"GET",
		"/verify",
		bytes.NewReader(
			[]byte(requestBody),
		),
	)

	userStore := &InMemoryUserIDStore{
		ids: make(map[uint32]bool),
	}
	response := httptest.NewRecorder()

	handler := &VerifyUserHandler{
		secret: "johnny",
		db:     userStore,
	}

	handler.ServeHTTP(response, request)

	status := response.Result().StatusCode
	if status != http.StatusCreated {
		t.Error(
			fmt.Sprintf(
				"Invald status code returned: %d - %s",
				status,
				http.StatusText(status),
			),
		)
	}
}

func TestHTTPServeInvalidSecret(t *testing.T) {
	requestBody := `{
"pull_request": {
    "user": {
        "id": 3
     },
     "merged": true
},
"action": "closed",
"secret": "not johnny"
}`

	request := httptest.NewRequest(
		"GET",
		"/verify",
		bytes.NewReader(
			[]byte(requestBody),
		),
	)

	userStore := &InMemoryUserIDStore{
		ids: make(map[uint32]bool),
	}
	response := httptest.NewRecorder()

	handler := &VerifyUserHandler{
		secret: "johnny",
		db:     userStore,
	}

	handler.ServeHTTP(response, request)

	status := response.Result().StatusCode
	if status != http.StatusUnauthorized {
		t.Error(
			fmt.Sprintf(
				"Invald status code returned: %d - %s",
				status,
				http.StatusText(status),
			),
		)
	}
}
