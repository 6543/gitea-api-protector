package main

import (
	"encoding/binary"

	"go.etcd.io/bbolt"
)

type UserIDStore interface {
	HasUser(ID uint32) bool
	AddUser(ID uint32) error
}

var (
	userIDBucket []byte = []byte{0}
)

type FileDatabase struct {
	db     *bbolt.DB
	dbPath string
}

func (d *FileDatabase) Init() error {
	db, err := bbolt.Open(d.dbPath, 0600, nil)

	if err != nil {
		return err
	}

	err = db.Update(func(tx *bbolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists(userIDBucket)

		return err
	})

	d.db = db

	return err
}

func (d *FileDatabase) HasUser(userID uint32) bool {
	var hasUser bool

	d.db.View(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket(userIDBucket)

		key := make([]byte, 32)
		binary.LittleEndian.PutUint32(key, userID)

		user := bucket.Get(key)

		if len(user) > 0 {
			hasUser = true
		}

		return nil
	})

	return hasUser
}

func (d *FileDatabase) AddUser(userID uint32) error {
	return d.db.Update(func(tx *bbolt.Tx) error {
		bucket := tx.Bucket(userIDBucket)

		key := make([]byte, 32)
		binary.LittleEndian.PutUint32(key, userID)

		return bucket.Put(key, []byte{0})
	})
}

func NewInMemoryUSerIDStore() *InMemoryUserIDStore {
	return &InMemoryUserIDStore{
		ids: make(map[uint32]bool),
	}
}

// User ID store that works without DB. Is not safe for concurrent use and should only be
// used for testing.
type InMemoryUserIDStore struct {
	ids map[uint32]bool
}

func (s *InMemoryUserIDStore) AddUser(ID uint32) error {
	s.ids[ID] = true

	return nil
}

func (s *InMemoryUserIDStore) HasUser(ID uint32) bool {
	_, ok := s.ids[ID]

	return ok
}
