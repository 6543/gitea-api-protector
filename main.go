package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func main() {
	argLen := len(os.Args)

	if argLen != 2 {
		fmt.Fprintln(os.Stderr, "Usage: gitea-api-protector CONFIG_FILE")
		return
	}

	configPath := os.Args[1]

	config, err := ConfigFromFile(configPath)

	if err != nil {
		log.Fatal(err)
	}

	db := &FileDatabase{
		dbPath: config.DatabasePath,
	}

	err = db.Init()

	if err != nil {
		log.Fatal(err)
	}

	verifyUserHandler := &VerifyUserHandler{
		db:     db,
		secret: config.Secret,
	}

	authenticateHandler := NewAuthenticateHandler(
		config.GiteaURL,
		db,
		config.ApiKeyCacheExipration*time.Second,
	)

	http.Handle("/verify", verifyUserHandler)

	http.Handle("/auth", authenticateHandler)

	log.Fatal(http.ListenAndServe(config.NetworkInterface, nil))
}
