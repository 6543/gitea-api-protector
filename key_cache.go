package main

import (
	"sync"
	"time"
)

type KeyCache struct {
	keys   map[string]*CachedKey
	keysMx sync.RWMutex
	quit   chan bool
}

func NewKeyCache(cleanupInterval time.Duration, expirationTime time.Duration) *KeyCache {
	cache := &KeyCache{
		keys: make(map[string]*CachedKey),
		quit: make(chan bool),
	}

	cache.CleanupWithInterval(cleanupInterval, expirationTime)

	return cache
}

func (c *KeyCache) HasKey(keyString string) bool {
	c.keysMx.RLock()
	key, ok := c.keys[keyString]
	c.keysMx.RUnlock()

	if ok {
		key.Hit()
	}

	return ok
}

func (c *KeyCache) AddKey(keyString string) {
	key := NewCachedKey()
	key.Start()

	c.keysMx.Lock()
	c.keys[keyString] = key
	c.keysMx.Unlock()
}

func (c *KeyCache) StopCleanup() {
	c.quit <- true
}

// Starts goruitine that periodically cleans up
func (c *KeyCache) CleanupWithInterval(interval time.Duration, expiration time.Duration) {
	runCleanup := make(chan bool)

	go func() {
		runCleanup <- true
	}()

	go func() {
		for {
			select {
			case <-c.quit:
				return
			case <-runCleanup:
				var keyStrings []string

				c.keysMx.RLock()
				for keyString, key := range c.keys {
					if time.Since(key.LastHit()) > expiration {
						keyStrings = append(keyStrings, keyString)
					}
				}
				c.keysMx.RUnlock()

				if len(keyStrings) > 0 {
					c.keysMx.Lock()
					for _, keyString := range keyStrings {
						delete(c.keys, keyString)
					}
					c.keysMx.Unlock()
				}

				go func() {
					time.Sleep(interval)

					runCleanup <- true
				}()
			}
		}
	}()
}
