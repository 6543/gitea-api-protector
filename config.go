package main

import (
	"errors"
	"io/ioutil"
	"strings"
	"time"

	"gopkg.in/yaml.v2"
)

type Config struct {
	Secret                string        `yaml:"secret"`
	DatabasePath          string        `yaml:"database_path"`
	GiteaURL              string        `yaml:"gitea_url"`
	NetworkInterface      string        `yaml:"network_interface"`
	ApiKeyCacheExipration time.Duration `yaml:"api_key_cache_expiration_time"`
}

func ConfigFromFile(filePath string) (*Config, error) {
	fileContents, err := ioutil.ReadFile(filePath)

	if err != nil {
		return nil, err
	}

	config := &Config{}
	err = yaml.Unmarshal(fileContents, config)

	if err != nil {
		return nil, err
	}

	if config.GiteaURL == "" || config.DatabasePath == "" || config.Secret == "" || config.NetworkInterface == "" {
		return config, errors.New(
			"At least one config parameter is not set. secret, database_path, gitea_url and network_interface must all be set",
		)
	}

	// Set the default cache expiration time for API keys to 20 minutes
	if config.ApiKeyCacheExipration == 0 {
		config.ApiKeyCacheExipration = 1200
	}

	// Make sure ther are no trailing slashes in the gitea URL
	config.GiteaURL = strings.TrimRight(config.GiteaURL, "/")

	return config, nil
}
